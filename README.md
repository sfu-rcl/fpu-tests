# FPU Instruction tests
This repository contains software designed to test the correctness of every floating-point (FP) instruction supported by the RV32FD extensions.

The tests are presented in software rather than hardware, which has the benefit of not just testing numerical correctness but also:
- FP instruction decoding
- FP memory operations
- FP register file read and write
- FP instruction issue, writeback and retire
- FP control and status registers

## Origins
This repository was created as a companion for testing the FPU expansion for [CVA5](https://github.com/openhwgroup/cva5).

Most of the test vectors originate from a combination of [Berkeley SoftFloat](http://www.jhauser.us/arithmetic/SoftFloat.html) and [Berkeley TestFloat](http://www.jhauser.us/arithmetic/TestFloat.html), compiled to implement the FP operators defined by the RISC-V F and D extensions.
Specifically, the command:
`./testfloat_gen -exact -tininessafter -$(ROUNDING_MODE) $(OPERATOR_NAME)` was used to invoke the test vector generator.

Several simpler instructions (FLE, FLT, FLE, FMIN, FMAX, FCLASS, FSGNJ, FSGNJN, FSGNJX, FMV.X.W, FMV.W.X) are not supported by TestFloat, so a reduced selection of manually-created test vectors have been provided.

Additionally, TestFloat does not provide operators for fused multiply-addition other than the standard FMADD rd = (rs1\*rs2)+rs3.
However, because the operands can have different signs, extensive testing of FMADD is sufficient to evaluate the arithmetic correctness of the FMSUB rd = (rs1\*rs2)-rs3, FNMSUB rd = -(rs1\*rs2)+rs3, and FNMADD rd = -(rs1\*rs2)-rs3 instructions.

# Using the test vectors
Each test case comprises the concatenation of:
- Instruction name with periods stripped (e.g. fadd, feq, fmvxw)
- Precision level
  - s = Single
  - d = Double
- Rounding mode
  - rtz = Round to zero
  - rup = Round to +$\infty$
  - rdn = Round to -$\infty$
  - rne = Round to nearest, tying to even
  - rmm = Round to nearest, tying to max magnitude

Note that not all instructions support different rounding modes or levels of precision.

## Test code
For each test case, a `.h` test driver is provided alongside a `.txt` file containing the test vectors.

### Text files
Each `.txt` file contains columns of test data in hexadecimal format. From left to right, the columns are: `rs1 rs2 rs3 rd fflags`

The `fflags` are the logical or of the following fields (from most significant bit to least): `invalid (nv), divide by zero (dz), overflow (of), underflow (uf), inexact (nx)`

The precise meaning and interpretation of the operands and results can be understood by reading the appropriate documentation in the RISC-V standard.
Depending on the instruction, the operands and result may be integer or FP numbers. Note that `rs2` and `rs3` are optional and are not present in all files.

### Converter
`converter.c` takes a single `.txt` file as an argument and converts all the data to raw binary format. It internally widens each field to 8 bytes for alignment purposes. This means that accessing every entry for every field results in an aligned 64-bit memory operation.

### Header files
The `.h` files contain a `struct` description of the fields present in the accompanying `.txt` file along with a single function that tests one combination of inputs.

The `struct` definition usually contains explicit padding such that each field in the `.txt` file is widened to 8 bytes. This serves as a reminder that the converter is internally widening each field and prevents any address alignment exceptions.

The test function is conceptually straightforward. It merely:
1. Clears any previous flags
2. Loads the test inputs
3. Uses inline assembly to invoke the correct instruction and rounding mode
4. Compares the observed output to the correct output (using integer comparisons)
5. Verifies the flags match the expected result

The code is structured in such a way that ensures that the compiler will not generate any FP instructions other than loads and stores and the one under test. Specifically, FEQ is not used to compare results, and FP stores followed by integer loads are used to move data from the FP registers to the integer registers rather than the FMV instruction.

### Top level wrapper
The top level C file, `fputest.c`, is responsible for including the necessary test files and iterating over all test cases. It either runs until successfull test completion, or exits at the first error after printing out the failed test number.

## Compiling the tests
Actually running this test code on a simulator or on actual hardware is beyond the scope of this explanation, but the principle steps to create an executable for an individual test are the following:
1. Extract the instructions: `tar -I 'xz -T0' -xf instructions.tar.xz`
2. Compile the converter: `gcc -Wall -O2 converter.c`
3. Set the test name: `INSN=faddsrtz`
4. Run the converter: `./converter $INSN.txt`
5. Convert the raw binary data to C header include format: `xxd -i converted.bin > $INSN.bin.h`
  - Including the test data can be done using alternative methods like `.incbin` in an assembly file, or using the new (as of C23) `#embed` preprocessing directive. The former requires a potentially more complicated compilation, and the latter requires an up-to-date compiler.
6. Set the `#include` string in `fputest.c`:
  - By manually replacing `#define INSN`
  - Using `sed -i "s/^#define INSN.*/#define INSN $INSN/" fputest.c`
  - Or during compilation with `-DINSN=$INSN`
7. Compile `fputest.c`, which will automatically `#include` `$INSN.h` and `$INSN.bin.h`

# Test listing
- FADD.D (rounding)
- FADD.S (rounding)
- FCLASS.D
- FCLASS.S
- FCVT.D.S
- FCVT.D.W
- FCVT.D.WU
- FCVT.S.D (rounding)
- FCVT.S.W (rounding)
- FCVT.S.WU (rounding)
- FCVT.W.D (rounding)
- FCVT.WU.D (rounding)
- FCVT.W.S (rounding)
- FCVT.WU.S (rounding)
- FDIV.D (rounding)
- FDIV.S (rounding)
- FEQ.D
- FEQ.S
- FLE.D
- FLE.S
- FLT.D
- FLT.S
- FMADD.D (rounding)
- FMADD.S (rounding)
- FMAX.D
- FMAX.S
- FMIN.D
- FMIN.S
- FMUL.D (rounding)
- FMUL.S (rounding)
- FMV.W.X
- FMV.X.W
- FSGNJ.D
- FSGNJN.D
- FSGNJN.S
- FSGNJ.S
- FSGNJX.D
- FSGNJX.S
- FSQRT.D (rounding)
- FSQRT.S (rounding)
- FSUB.D (rounding)
- FSUB.S (rounding)

