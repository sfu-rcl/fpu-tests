/*
 * Copyright © 2023 Chris Keilbart, Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Chris Keilbart (ckeilbar@sfu.ca)
 */

//Converts a file filled with columns of space seperated hexadecimal numbers to raw binary

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

//There is potential for "improvement" to this file, but the current design is largely sufficient
//1. Output to a provided file name
//2. Output to stdout if it is not a terminal
#define OUTPUT "converted.bin"

int main(int argc, char* argv[]) {
    char hex[17];
    uint64_t raw;

    if (argc != 2) {
        printf("Usage: %s filename\n", argv[0]);
        return 1;
    }

    FILE* fp = fopen(argv[1], "r");
    if (!fp) {
        printf("Error opening input file %s\n", argv[1]);
        return -1;
    }

    FILE* fd = fopen(OUTPUT, "wb");
    if (!fd) {
        printf("Error opening output file %s\n", OUTPUT);
        return -1;
    }

    while (fscanf(fp, "%17s", hex) != EOF) {
        raw = strtoull(hex, NULL, 16);
        //Always output 16 bytes for struct alignment
        fwrite(&raw, 8, 1, fd);
    }

    fclose(fp);
    fclose(fd);

    return 0;
}

