/*
 * Copyright © 2023 Chris Keilbart, Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Chris Keilbart (ckeilbar@sfu.ca)
 */

//General purpose top-level FPU test driver
#include <stdio.h>

//INSN.h and INSN.bin.h will be imported
#define INSN fsgnjs

//Macros for including the header and its data
#define STRINGIFY(x) #x
#define INCLUDE_FILE(x) STRINGIFY(x.h)
#define INCLUDE_DATA(x) STRINGIFY(x.bin.h)
#include INCLUDE_FILE(INSN)
_Alignas(double)
#include INCLUDE_DATA(INSN)

//Macros for extracting the data from the header
//#define MERGE(x, y) x ## y
//#define MERGE2(x, y) MERGE(x, y)
//This would expand to INSN_txt
//#define DATA_NAME MERGE2(INSN, _txt)
#define DATA_NAME converted_bin

int main() {
    const unsigned num_tests = sizeof(DATA_NAME)/sizeof(test_t);
    test_t* casted = (test_t*) DATA_NAME;

    for (unsigned i = 0; i < num_tests; i++) {
        if (test(&casted[i])) {
            printf(INCLUDE_FILE(INSN)" failed test %u.\n", i+1);
            return -1;
        }
    }
    printf(INCLUDE_FILE(INSN)" tests passed\n");
    return 0;
}

